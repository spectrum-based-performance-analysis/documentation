% !TeX spellcheck = en_US

\chapter{Evaluation}
\label{chap:evaluation}
To verify the results of the experiments and to assess the approach of spectrum-based analysis for performance problem localization the obtained results have to be evaluated. We choose the application performance monitoring tool \textit{Kieker} to verify our results. After getting the results (described in \cref{chap:results}), they needed to be interpreted in order to find out the location of performance problems. The performance problem locations found by spectrum-based performance problem analysis were then compared to the root causes found by using \textit{Kieker} to analyze the programs. \\
Using the results from the experiment of sort algorithm and the webapp \textit{ JPetStore}, spectrum-based analysis provides information about the causing element on the level of code blocks. In contrast to that the analysis tool \textit{Kieker} only provide the results on the method level. So to make both results comparable, results of spectrum-based analysis need to be abstracted to method level.


\section{Sort Algorithms}
\label{sec:eval_sort}
In \cref{sec:results_sort_algo} it was shown that all sort algorithms
had highly ranked code blocks. For branch hit spectra the highest risk
values for each block were even identical for all algorithms. This can
be explained by the fact, that in this simple program there is no
difference which blocks are executed depending on the input. Therefore
all code blocks appear to contribute the same to the performance
problem. Because there is only one set of input data that exhibits a
performance problem it is also obvious that the risk values can not be
very high (highest is 0.58). In this instance the branch hit spectra
analysis did not help localizing the performance problem at all. From
this we conclude that branch hit spectra can only localize performance
problems as far as they are related to a certain path being taken.   \\
The branch count spectra allow to see an ordering in how much each
sorting algorithm contributes to the performance problem. Bubble sort
has the highest chance to contain the performance problem, after that
follow selection sort and merge sort which also get a very high risk
value. The risk value of insertion sort is already so much less that
it does not fall into the code blocks examined in detail on the first
try.
\\
The analysis of the program with \textit{Kieker} shows a different situation. A
diagram containing only the sort algorithms and their running time is
shown in \cref{fig:kieker_sort}. Bubble sort takes 859ms which is by
far the biggest share of time. It contains the actual performance
problem. Selection sort consumes slightly more time (137ms)
than insertion sort (101ms). In contrast to the other algorithms, merge sort
takes virtually no time ($< 1$ms).

\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{KiekerAssemblyOperationDependencyGraph_Sort_only}
  \caption{Result of the runtime analysis of the sort algorithms with
    kieker. Each depicted bubble represents a method. Below the
    method name minimum, maximum, average and total runtime can be seen.  \label{fig:kieker_sort} }
\end{figure}

When comparing the analysis made with \textit{Kieker} to the results of the
branch count spectra analysis, several things can be noticed. Bubble
sort was identified correctly as most likely containing the
performance problem. Also, selection sort was correctly identified as
contributing second most to the performance problem. The risk value of
merge sort is too high and a false positive. The high risk value
results from the code block being executed ``often'' in runs which
exhibit a performance problem. Here ``often'' is meant in relation to
the number of executions in not performance problem exhibiting
runs. This metric does not seem to be a reliable indicator for
performance problems as there could be many code blocks that have a
very small running time and are executed ``often''. An approach to
improve the results of the analysis with branch count spectra could be
to design a similarity coefficient which also uses the relation to the
number of executions of the code block with the maximum number of
executions.

\section{JPetStore}
For the second experiment branch hit spectra have proven to be well-suited. The results, however, are totally dependent on the similarity coefficients used to analyze the spectra. Two of the four risk formulas, namely \textit{ Ochiai } and \textit{ Jaccard}, produced really good results. They both assigned a risk value of 1 to the code block actually containing the artificial performance problem, while assigning significantly lower risk values to every other code block. Since risk values all range from 0 to 1, these values are the maximum risk value that can be assigned. A risk value of 1 theoretically means that there is a 100 \% guarantee the corresponding code block contains the problem, at least if there is no other code block with a risk value of 1. An explanation for these values is that the code block's spectrum vector and the problem vector are not only similar, but exactly the same. If two of these vectors are the same, the code block containing the performance problem was executed \textit{only} in those runs which contain a performance problem. There could hardly be a result more unambiguous than this result. In contrast to this the other two risk formulas, \textit{Russel \& Rao} and \textit{ Binary}, have generated risk values that are nearly useless for deriving any kind of statement about the location of the performance problem. Both formulas assign the same risk value, which in this case is also the highest, to more than half of all code blocks taking part in the experiment. Maybe some code blocks with the lowest risk values can be excluded to not contain the performance problem, but there are still too many code blocks having the same risk values making it impossible to find a good starting point of where to start looking for the problem.

For branch count spectra the results have not been as clear as with branch hit spectra, but still very good. Both similarity coefficients, \textit{Cosine Similarity} and the modified version of \textit{ Jaccard}, show similar results. Although the absolute values of the two formulas differ a lot, the qualitative result is very similar. Both formulas assign the highest risk value to the code blocks actually containing the performance problem and assign significantly lower values to any other code block. The risk values again implicate a very clear statement about the location of the performance problem.

The Analysis with \textit{Kieker} confirmed that the method in which the wait statement was injected took nearly all of the processing time.

Therefore branch hit spectra with the similarity coefficients \textit{ Ochiai } and \textit{ Jaccard } and branch count spectra with the \textit{Cosine Similarity} and the modified \textit{ Jaccard } seem to be really good at locating a performance problem that is based on a single command needing a long period of time to return.