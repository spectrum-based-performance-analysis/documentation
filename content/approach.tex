% !TeX spellcheck = en_US

\chapter{Approach}
\label{chap:approach}
This chapter contains a description of the techniques and tools that were used in the experiments as well as the programs that were inspected. In \cref{sec:research_method} the research question is stated. Afterwards the choice of spectra types, similarity coefficients and implementation tools is explained. \cref{sec:experiment_setup} gives more details about the programs used and how the results will be evaluated. The repositories containing this paper as well as the implemented tool and the results can be found at \url{https://gitlab.com/spectrum-based-performance-analysis}.
	
\section{Research Method}
\label{sec:research_method}
The research question examined by this work is: ``In which cases does which spectrum type with which similarity measure recognize performance problems best?'' The goals of this work follow from this research question. The following list is showing the individual goals in detail:
\begin{itemize}
	\item Find suiting spectrum types to apply for spectrum-based performance problem localization.
	\item Find suiting similarity coefficients to apply for spectrum-based performance localization.
	\item Find correlation of measurements from the experiments to the actual location of performance problems.
	\item Find suiting scenarios in which it is useful to apply spectrum-based analysis to performance problem localization.
\end{itemize}

There are three main activities that needed to take place to obtain the results of this work:
\begin{enumerate}
	\item Production of spectra
	\item Analysis of spectra
	\item Evaluation
\end{enumerate}

\subsection{Which spectrum types fit best?}
Harrold et al. \citep{harrold2000empirical} analyzed and evaluated a lot of different spectra types (see \cref{tab:spectraTable}). These spectra types have been evaluated based on two different measures: imprecision and unsafety. The degree of imprecision represents the rate of false positives. The more non-existent faults are revealed by the spectrum the more imprecise the spectrum type gets. The degree of unsafety represents the rate of false negatives. The more faults there are that don't correlate with a spectrum difference, i.e. are not revealed by a certain spectrum type the unsafer the spectrum type gets. Based on these two measures the most interesting spectrum types have been branch hit/count spectra and path hit/count spectra. Most of the other spectra had bad values in at least one of the two measures. The complete path spectrum had nearly identical results for imprecision and unsafety as the path count and branch count spectra but has a lot more overhead and therefore won't be applied in this work. The branch hit and path hit spectra perform nearly identical, as do the branch count and path count spectra. While both hit spectra have a very low imprecision they are a bit more unsafe than the corresponding count spectra, since the hit spectra contain only binary information and therefore contain less information than the count spectra. These results are based on regression tests, with programs containing only a single defect. \citep{harrold2000empirical}\\
Considering these results branch hit and branch count spectra are chosen for the experiment. Since branch hit / count and path hit / count spectra produced nearly identical results and branch spectra are a little bit simpler to obtain, path hit / count spectra won't be used.

\subsection{Which similarity coefficients fit best?}
\label{sub:bestSimCoeff}
There are many different similarity measures and a good deal of them have been analyzed and evaluated in \citep{xie2013theoretical}. Five out of 30 similarity measures have been identified as best. These five measures are:
\begin{itemize}
	\item Russel \& Rao
	\item Binary
	\item Naish1
	\item Naish2
	\item Wong1
\end{itemize}
However, the resulting values of three out of these five measures (\textit{Naish1}, \textit{Naish2}, \textit{Wong1}) range not only from 0 to 1, but may also result in values above 1. This means that these similarity measures are harder to compare to each other since they don't have the same range. Therefore only \textit{Russel \& Rao} and \textit{Binary} out of these 30 measures will be used. Additionally two more similarity measures will be used: \textit{Ochiai} and \textit{Jaccard}. They are interesting because they have already been used in some scientific works dealing with spectrum-based fault localization \citep{abreu2009practical, xie2013theoretical, xie2010spectrum} and seem to be two of the more established similarity measures for fault localization. The problem with all of the above similarity measures is their limitation to comparing binary vectors only. This limits their usage to hit spectra. Count spectra need to be analyzed with other similarity measures. Therefore another two similarity measures are introduced: \textit{Cosine Similarity} and another, more universal version of \textit{Jaccard}. To sum it up, six different similarity measures are used:

\subsubsection{Similarity Coefficients for Hit Spectra}
\begin{itemize}
	\item Ochiai
	\begin{equation}s_{\text{Ochiai}}(j) = \frac{a_{11}(j)}{\sqrt{(a_{11}(j) + a_{01}(j))\cdot (a_{11}(j) + a_{10}(j))}}\end{equation}
	\item Jaccard (binary version)
	\begin{equation}s_{\text{JaccardBinary}}(j) = \frac{a_{11}(j)}{a_{11}(j) + a_{01}(j) + a_{10}(j)}\end{equation}
	\item Russel \& Rao
	\begin{equation}s_{\text{Russel \& Rao}}(j) = \frac{a_{11}(j)}{a_{11}(j)+a_{01}(j)+a_{10}(j)+a_{00}(j)}\end{equation}
	\item Binary
	\begin{equation}s_{\text{ Binary }}(j) = \left\{ \begin{array}{ll}
	0, & a_{11}(j) < F \\ 1, & a_{11}(j) = F
	\end{array}\right.\end{equation}
\end{itemize}
	
With
\begin{itemize}
	\item $a_{11}(j)$: number of failed runs in which code block $j$ was executed
	\item $a_{01}(j)$: number of failed runs in which code block $j$ was NOT executed
	\item $a_{10}(j)$: number of passed runs in which code block $j$ was executed
	\item $a_{00}(j)$: number of passed runs in which code block $j$ was NOT executed
	\item $F$: number of failed runs, $F = a_{11}(j)+a_{01}(j)$
\end{itemize}
	
	
\subsubsection{Similarity Coefficients for Count Spectra}
\begin{itemize}
	\item Cosine Similarity
		\begin{equation}s_{\text{Cosine Similarity}}(x,y) = \frac{\sum_{i=1}^{n}x_i\cdot y_i}{\sqrt{\sum_{i=1}^{n}x_i^2}\cdot\sqrt{\sum_{i=1}^{n}y_i^2}}\end{equation}
	\item Jaccard (universal version)
		\begin{equation}s_{\text{ JaccardUniversal }}(x,y) = \frac{\sum_{i}min(x_i,y_i)}{\sum_{i}max(x_i,y_i)}\end{equation}
\end{itemize}
With $x=(x_1, x_2, ..., x_n)$ and $y=(y_1, y_2, ..., y_n)$.\\

Important to note here, there is a small adjustment to the universal version of the \textit{ Jaccard } measure. For count spectra the code block vectors may contain any natural numbers but the problem vector is still binary. For the \textit{ Cosine Similarity } it makes no difference since it can be visualized as the angle between both vectors. The smaller the angle between the two vectors the more similar the two vectors are. The absolute value of the vectors, i.e. their length have no influence on the similarity values. But \textit{ Jaccard } uses maximum values and this produces useless values in this case. Therefore the binary vector is upscaled: every "1" in the problem vector is replaced by the maximum value from the code block vector. If for example the code block vector is $(6,2,7)$ the maximum value is $7$ and if the problem vector is $(1,0,1)$ the upscaled problem vector for \textit{ Jaccard } would be $(7,0,7)$.

\subsection{How to use spectrum-based analysis for localizing performance problems}
The approach for combining spectrum-based analysis with the localization of performance problems is pretty simple and works similar to spectrum-based fault localization, which is already used to find faults in source code. The problem vector (or error vector) (see \cref{fig:spectrumMatrix}) is now used as a performance problem vector. The simplest way to apply this is having a binary vector, just like in spectrum-based fault localization. A "1" in the performance problem vector means that there is a performance problem in the corresponding run and a "0" accordingly means that there is no performance problem in that run. This definition leads to the need to know in advance if there is a performance problem in a specific run. This approach is not able to discover performance problems but rather tries to localize performance problems that are already known.

	
\subsection{How the instrumentation works}
For the experiments conducted for this paper programs written in Java were used. Therefore the Java code had to be instrumented in some way. There are many open source tools available being able to instrument a program which can be summed up as profiler tools. However, these tools are not suitable for the experiments because they just record hit spectra only and it is very difficult respectively impossible to obtain the code block identifiers that correspond to the individual spectrum values. Therefore a dedicated instrumentation tool had to be implemented for the experiments. A fitting tool to do the instrumentation had been found in \textit{Javassist} \citep{javassist}.
\textit{ Javassist } is a java library which makes it possible to manipulate the Java byte code of an application during runtime. Although \textit{ Javassist } operates on byte code level, a source-level API is provided which enables to specify the instrumentation tasks on source code level. New classes can be introduced and existing classes can be modified. With this tool it is possible to instrument the applications examined in the experiments, without changing the source code of these applications. Since the code blocks that had to be identified are on branch-level, the individual branches in the code had to be located and code for flags and counters had to be inserted at every branch. These flags and counters are tracking whether the individual branches have been executed or not (branch hit spectra) respectively how often the individual branches have been executed (branch count spectra). Additionally, the branch identifiers had to be tracked to clearly relate the flags and counters to the individual code blocks. Then the tracked data had to be collected and stored so that it can be analyzed later.

\section{Experiment Setup}
\label{sec:experiment_setup}
Two different experiments have been executed, analyzed and evaluated. The setup and the approach to the evaluation of both experiments are explained in detail in the following sections.

\subsection{Sort Algorithms Experiment}
For the first experiment implementations of a selection of sort algorithms have been used. The idea here is to choose sort algorithms that have very different runtime complexities. Therefore it might be possible to identify the slower algorithms by analyzing and evaluating the spectra for some runs with different input data. The sort algorithms chosen for this experiment are:
\begin{itemize}
	\item Bubble Sort $(O(n^2))$
	\item Selection Sort $(O(n^2))$
	\item Insertion Sort $(O(n^2))$
	\item Merge Sort $(O(n \log n))$
\end{itemize}
For the input data some randomly generated lists of integer values are used, of which all but one are unsorted. The lists have varying lengths to cover different kinds of data sizes. Hit and count spectra are generated by running every of the aforementioned sort algorithms on every input data set. The performance problem vector is set after executing the experiment by assigning "true" to the run with the longest unsorted list. While executing the experiment hit or count values (depending on the spectrum) are assigned to every code block of every sort algorithm implementation. Finally risk values are assigned to every code block of every sort algorithm implementation by applying the six similarity coefficients described in \cref{sub:bestSimCoeff}.

It is expected that high risk values are assigned to code blocks of inefficient sort algorithms, like bubble sort, and that low risk values are assigned to code blocks of more efficient sort algorithms, like merge sort.

\subsection{Real World Application Experiment with JPetStore}
\textit{ JPetStore } is a simple web application which is kept on the essential of web programming. By only using Java and SQL to achieve a web store, the publisher of \textit{ JPetStore } wants to provide a simple entry point in programming a web application with less programming skills \cite{MyBatis}. Due to the simplicity of the web store, \textit{ JPetStore } is also used as test application for analysis software. Therefore \textit{ JPetStore } is a good choice as test application to use spectrum-based analysis for a nearly real world web application. The code of \textit{ JPetStore } is freely available in a git-repository, so it can be downloaded and the code can be modified easily. For the test purpose of spectrum-based analysis, a performance problem inside the application is needed. To inject a performance problem, the simplest way is inserting a wait-statement into the code.  \\
As the runs used to obtain the spectra have to contain user interaction, a method is needed to guarantee exactly identical behavior. The test automation framework \textit{Selenium} was used to make the tests on the web application repeatable. \textit{Selenium} is an open source tool which enables automating a sequence of actions, like clicks, in a web browser \cite{Selenium}. To test \textit{ JPetStore } it is sufficient to use the browser plug-in for Firefox. The plug-in records every click on the website during the user interface tests. Recorded sequences of clicks can be replayed from the test framework at any time. All interface test were defined in test cases which cover the main functionality of the application. \\
To analyze the program behavior and find performance problems in \textit{ JPetStore}, it is enough to define some test cases with and without performance problems, covering the main functionality of the web shop. \cref{tab:SeleniumTestCases} gives an overview of the defined test cases for \textit{ JPetStore}. The preconditions are fulfilled by running the tests in a combined way. So the spectrum of test case T4 also includes all the code blocks executed for test case T3.  A wait-statement was introduced into the source code responsible for adding an item into the shopping cart. So test cases T5 to T8 contain a performance problem. All test cases were run on the unmodified and the modified version of \textit{ JPetStore}.


\begin{table}
	\centering
	\begin{tabular}{lll}
		\toprule
		\textbf{No.} & \textbf{Test Case} & \textbf{Remark} \\
		\midrule
		T1 & Look up the price of a golden retriever & Navigate through the web shop \\
		T2 & Search for a goldfish & Use the Search function\\
		T3 & Register on the web shop & Create a new Account\\
		T4 & Change the password of the account & Precondition: T3\\
		T5 & Add a goldfish to the shopping cart & Precondition: T3 \\
		T6 & Order 30 reptiles & Add reptile to shopping cart and\\ & & change number of order \\ & & Precondition: T3, T5\\
		T7 & Reverse the order of 30 reptiles & Remove the reptile from shopping cart \\ & &  Precondition: T3, T6\\
		T8 & Send order for one goldfish & Proceed the checkout\\ & & Precondition: T3,T5\\
		T9 & Search for a dog & Precondition: T3\\
		T10 & Sign out & Log out the account \\ & & Precondition: T3\\
		\bottomrule
	\end{tabular}
	\caption{Overview of defined test cases for \textit{ JPetStore } }
	\label{tab:SeleniumTestCases}
\end{table} 


\subsection{Evaluating the experiment results with Kieker}
After having executed both experiments, the results of these experiments need to be double-checked by comparing them to the results an established performance monitoring tool produces on the same applications. The tool used for this evaluation is \textit{Kieker} \citep{hoorn12_kieker}, an open source tool for monitoring the performance of a software system by applying dynamic analysis. \textit{Kieker} is able to generate various graphs, e.g. operation dependency graphs. Such graphs show which method or component is calling which method or component and the elapsed time in the individual methods or components. These results can be compared manually with the results from the two experiments. If the analysis with \textit{Kieker} reveals similar results, the results from the two experiments most probably are valid.
