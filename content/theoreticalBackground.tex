% !TeX spellcheck = en_US

\chapter{Theoretical Background}
\label{chap:theoreticalBackground}
The foundation for the work of this paper is discussed in this
chapter. \cref{sec:perf_prob} gives an introduction on what performance
problems are and why it is hard to find and correct them. The building
blocks and necessary steps of spectrum-based analysis are described in
\cref{sec:sba}.


\section{Performance Problems}
\label{sec:perf_prob}
Apart from software quality characteristics such as functionality, usability and security, performance is an important quality characteristic as well. It describes the time behavior, resource utilization and the capacity of a software system. Measurable properties of software to analyze performance are called Key Performance Indicators (KPI). Ian Molyneaux, an expert in performance testing, groups the KPIs into two main categories: efficiency-oriented and service-oriented indicators. The efficiency-oriented indicators, like the use of resources and data throughput, describe the velocity of a software system. In contrast to this, the service-oriented indicators describe the availability of resources and response time. So performance in the context of quality aspects includes velocity and optimization of resource utilization. \citep{Kemena2009}
\par Analyzing and evaluating performance is very difficult and different for every software environment. The performance of a software system depends on various system characteristics making it difficult, expensive and time consuming to evaluate the system's performance. Meanwhile some test methods have been established for measurement of performance characteristics. Baseline tests, load tests, stress tests and stability tests are examples for techniques that can be used as a starting point to determine performance problems. The problem is not to observe a performance problem but to localize the bottlenecks. 

\section{Spectrum-based Analysis}
\label{sec:sba}
Spectrum-based analysis makes use of program spectra to derive more information about program behavior during run-time \citep{abreu2009practical}. Comparing program spectra of different runs or versions provides additional information. Spectrum-based analysis is often used to localize problems.

\subsection{Program Spectra}
\par A program spectrum is a collection of run-time specific data describing the dynamic behavior of a program. There are different types of program spectra each providing different results and track different aspects of program behavior:

\begin{description}
\item[Branch Spectra] Conditional branches result from the decisions taken in control flow statements like if- or switch-statements. So a branch is the outcome of a decision. A branch spectrum records whether branches were exercised during execution of the program or not.
\item[Complete Path Spectra] In contrast to a branch spectrum, a complete path spectrum represents the whole execution path of a program.
\item[Path Spectra] Because usually only some paths of the execution of the program are interesting, there is no need to record a complete path spectrum. Therefore, path spectra are used to only consider spectra tracking partial paths. The set of paths for a path spectrum can be obtained by an acyclic control flow graph of the program. 
\item[Data-Dependence Spectra] Considering definition-use pairs of variables, conclusions on the program behavior can be drawn. For this reason data-dependence spectra track the set of exercised definition-use pairs.
\item[Output Spectra] As the name indicates, output spectra capture the output from the execution.
\item[Execution Trace Spectra] An execution trace spectrum records the program execution in the most detailed manner. It monitors the sequence of all executed statements. Hereby an execution trace spectrum represents all executed instructions along program paths.
\end{description}

The different spectra can be used for different purposes. The main difference between all these spectrum types is the different level of detail a program execution is recorded. Where a complete path spectrum records the executed paths of the program, an execution trace spectrum records on the most fine grained level: the executed statements themselves. In addition, some types of program spectra can be further subdivided. \cref{tab:spectraTable} summarizes all the different types of program spectra and shows the subdivision of branch spectra, path spectra and data-dependence spectra into hit and count spectra. A hit spectrum only indicates whether a branch, path or definition-use pair was exercised or not \citep{harrold2000empirical}. Count spectra also show, how often a branch, path or definition-use pair was exercised during the program run  \citep{harrold2000empirical}.

\begin{table}
  \centering
  \begin{tabular}{l|l}
  \toprule
  \textbf{Name}& \textbf{Description} \\
  \midrule
  Branch Hit Spectra & conditional branches that were executed \\
  Branch Count Spectra & number of times each conditional branch \\ & was executed\\
  Complete Path Spectra & complete path that was executed \\
  Path Hit Spectra & intraprocedural, loop-free path that was executed \\
  Path Count Spectra & number of times each intraprocedural, \\ & loop-free path was executed \\
  Data-dependence Hit Spectra & definition-use pairs that were executed \\
  Data-dependence Count Spectra & number of times each definition-use pair \\ & was executed \\
  Output Spectra & output that was produced \\
  Execution Trace Spectra & execution trace that was produced \\

  \bottomrule
  \end{tabular}
  \caption{A catalog of program spectra  \citep{harrold2000empirical}}
  \label{tab:spectraTable}
\end{table}

\subsection{Instrumentation} To record the different spectra during a program run, the program needs to be instrumented before. Instrumentation describes the process of inserting code flags at insertion points into a program \citep{fischmeister2009time}. The insertion points differ for each type of program spectrum. Tools and techniques, which are already available to instrument programs for other purposes like debugging, can be used. For example a path profiler instruments programs so that during execution a path spectrum is produced. For the different types of spectra different techniques and tools are needed and sometimes new approaches need to be considered to instrument a program for specific purposes.

There are several ways to instrument a program. An instrumentation that is done on source-code level is called a source-to-source transformation. Furthermore instrumentation can be part of the compilation process. Therefore, the compiler needs to be modified so it injects the instrumentation instruction at specific, well-defined insertion points. Another way to instrument programs is on the object-code-level or to define a post-loader that modifies files. \citep{reps1997use}

Fundamentally an instrumentation is processed in three stages. First of all a source analysis needs to take place. The source-analyzer breaks the program down into basic blocks on the basis of a defined criteria. For program spectra a criteria to break down program code is the spectrum type. For a branch spectrum the code is divided into the different possible branches. Out of these basic blocks a call graph is created. It is important that this call graph is acyclic. A cyclic graph needs to be transformed into an acyclic graph. \cref{fig:controlFlowGraph} shows such an acyclic call graph of a program fragment where the basic blocks are branches.
In the next stage, the actual instrumentation takes place. For every carved out basic block from the source analysis instrumentation code is injected. The instrumentation code can be custom defined. Often, a flag at the beginning of each basic block is set, containing some identifier.
In the last stage the collection of the exercised basic blocks needs to be specified. For each hit spectrum it is sufficient to record the exercised basic blocks. A count spectrum also needs to record the number of executions. The instrumented program is now ready for execution and the trace of exercised basic blocks can be collected.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{controlFlowGraphv2.png}
  \caption{A program fragment and its control-flow graph \citep{reps1997use}}
  \label{fig:controlFlowGraph}
\end{figure}

The instrumentation of a program is an essential step in spectrum-based analysis. It defines the collected data sets for further analysis and differentiates between the different types of program spectra. 

\subsection{Analysis with Similarity Coefficients} 
Once the spectra have been generated, they need to be analyzed. In this method a risk value is assigned to every code block in the program. A code block can be any source code unit, e.g. class, method, statement, branch, etc. A risk value gives information about the probability of an individual code block to be the root cause of the problem that needs to be found. In many cases these risk values range from 0 to 1. After assigning the risk values to the individual code blocks a priority list of all the code blocks can be created. The code block with the highest risk value gets the highest priority, the code block with the second highest risk value gets the second highest priority, and so on. Then the code blocks are manually examined in detail, one by one, beginning at the code block with the highest priority. The goal here is that only a few code blocks need to be examined in detail before finding the root cause of the problem. \citep{abreu2009practical, xie2013theoretical}
\\
With similarity coefficients it is possible to assign risk values to code blocks. In general, similarity coefficients can be used to compare two vectors containing the same amount of elements and find out how similar these vectors are. Both vectors can be binary or contain any kind of numbers. For spectrum-based analysis a spectrum matrix containing all the values from all the spectra and an error vector need to be constructed. \cref{fig:spectrumMatrix} shows how the spectrum matrix and the error vector look like. Every row $M$ in the matrix represents a run of the program under test and every column $N$ represents a code block or program part. Therefore an element $x_{MN}$ represents the spectrum value that has been tracked during recording spectrum $M$ for code block $N$. In case of a hit spectrum all these elements would be binary, in case of a count spectrum these elements would contain natural numbers. The error vector is usually binary, containing the information whether or not the individual runs $M$ reveal a problem in the program. \citep{abreu2009practical}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{spectrumMatrix.png}
	\caption{A spectrum matrix and an error vector \citep{abreu2009practical}}
	\label{fig:spectrumMatrix}
\end{figure}

Now every column $N$ in the spectrum matrix can be compared to the error vector by applying similarity coefficients. The column with the highest similarity to the error vector represents the code block with the highest risk value. There is a large amount of different similarity coefficients and therefore many different formulas to calculate the risk values exist. There has already been going a lot of effort into research for finding the best similarity coefficients for spectrum-based analysis \citep{xie2013theoretical}. Similarity coefficients that have already been considered and proven to be well-suited are for example \textit{Ochiai} and \textit{Jaccard} \citep{abreu2009practical, xie2010spectrum}.



 
